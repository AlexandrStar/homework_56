import React from 'react';

const Button = props =>  <button disabled={props.disabled}
                                 onClick={props.click}
                                 className={`btn-board ${props.class} ${props.showRing}`}>{props.children}</button>;

export default Button;